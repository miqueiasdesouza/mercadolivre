<?php

namespace Miqueiasdesouza\Mercadolivre;

use Illuminate\Support\Facades\Facade;

class MercadoLivreFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'mercadolivre';
    }
}
