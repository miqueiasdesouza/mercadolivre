<?php

namespace Miqueiasdesouza\Mercadolivre;

use Illuminate\Support\ServiceProvider;

class MercadoLivreServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('mercadolivre', function ($app) {
            return new MercadoLivre($app['session'], $app['config'], $app['log'], $app['validator']);
        });
    }

    public function boot()
    {

    }
}
